package com.example.todoer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

@SpringBootTest(webEnvironment = NONE)
class TodoerApplicationTests {

    @Autowired Repo repo;
    @Autowired TodoerApi api;

    @BeforeEach
    public void setup() {
        System.out.println("    ----    api is : " + api.getClass().getSimpleName());
        repo.deleteAll();
    }

    @Test
    void testCreateAndGet() {
        //given
        api.createTodo("adam", new NewTodoRequest("buy milk"));

        //when
        var todos = api.getTodos("adam");

        //then
        assertThat(todos)
                .usingElementComparatorIgnoringFields("id")
                .containsExactlyInAnyOrder(
                        Todo.builder().user("adam").description("buy milk").build()
                );
    }

    @Test
    void testMarkDone() {
        //given
        long buyMilkId = api
                .createTodo("adam", new NewTodoRequest("buy milk"))
                .getId();

        //when
        api.markDone("adam", buyMilkId);
        var todos = api.getTodos("adam");

        //then
        assertThat(todos).isEmpty();
    }
}