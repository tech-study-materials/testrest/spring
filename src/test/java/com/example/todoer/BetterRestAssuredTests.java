package com.example.todoer;

import io.restassured.http.ContentType;
import io.restassured.mapper.TypeRef;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.when;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class BetterRestAssuredTests {

	@Autowired WebApplicationContext webApplicationContext;
	@Autowired Repo repo;

	@BeforeEach
	public void initializeRestAssuredMockMvcWebApplicationContext() {
		RestAssuredMockMvc.webAppContextSetup(webApplicationContext);
		repo.deleteAll();
	}

	@Test
	void testCreate() {
		given()
				.body(new NewTodoRequest("buy milk"))
				.contentType(ContentType.JSON)
		.when()
				.post("/adam/todo")
		.then()
				.statusCode(HttpStatus.SC_OK);
	}

	@Test
	void testCreateAndGet() {
		given()
				.body(new NewTodoRequest("buy milk"))
				.contentType(ContentType.JSON)
				.post("/adam/todo");

		var todos = when()
				.get("/adam/todo").as(new TypeRef<List<Todo>>() {});

		assertThat(todos)
				.usingElementComparatorIgnoringFields("id")
				.containsExactlyInAnyOrder(
						Todo.builder().user("adam").description("buy milk").build()
				);
	}

	@Test
	void testMarkDone() {
		long buyMilkId = given()
				.body(new NewTodoRequest("buy milk"))
				.contentType(ContentType.JSON)
				.post("/adam/todo")
				.as(IdResponse.class)
				.getId();

		when()
				.delete("/adam/todo/" + buyMilkId);

		var todos = when()
				.get("/adam/todo").as(new TypeRef<List<Todo>>() {});

		assertThat(todos).isEmpty();
	}
}
