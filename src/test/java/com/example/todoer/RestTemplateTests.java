package com.example.todoer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class RestTemplateTests {

	@Autowired TestRestTemplate template;
	@Autowired Repo repo;

	@BeforeEach
	public void setup() {
		repo.deleteAll();
	}

	@Test
	void testCreate() {
		var res = template.postForEntity("/adam/todo", new NewTodoRequest("buy milk"), Object.class);

		assertThat(res.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Test
	void testCreateAndGet() {
		//given
		template.postForObject("/adam/todo", new NewTodoRequest("buy milk"), Object.class);

		//when
		Todo[] todos = template.getForObject("/adam/todo", Todo[].class);

		//then
		assertThat(todos)
				.usingElementComparatorIgnoringFields("id")
				.containsExactlyInAnyOrder(
						Todo.builder().user("adam").description("buy milk").build()
				);
	}

	@Test
	void testMarkDone() {
		//given
		long buyMilkId = template
				.postForObject("/adam/todo", new NewTodoRequest("buy milk"), IdResponse.class)
				.getId();

		//when
		template.delete("/adam/todo/" + buyMilkId);
		Todo[] todos = template.getForObject("/adam/todo", Todo[].class);

		//then
		assertThat(todos).isEmpty();
	}
}
