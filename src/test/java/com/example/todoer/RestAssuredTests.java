package com.example.todoer;

import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.context.WebApplicationContext;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.when;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class RestAssuredTests {

	@Autowired WebApplicationContext webApplicationContext;
	@Autowired Repo repo;

	@BeforeEach
	public void initializeRestAssuredMockMvcWebApplicationContext() {
		RestAssuredMockMvc.webAppContextSetup(webApplicationContext);
		repo.deleteAll();
	}

	@Test
	void testCreateTodo() {
		given()
				.body("{\"description\":\"buy milk\"}")
				.contentType(ContentType.JSON)
		.when()
				.post("/adam/todo")
		.then()
				.statusCode(HttpStatus.SC_OK);
	}

	@Test
	void testCreateAndGet() {
		given()
				.body("{\"description\":\"buy milk\"}")
				.contentType(ContentType.JSON)
				.post("/adam/todo");

		when()
				.get("/adam/todo")
		.then()
				.body("$.size()", is(1))
				.and()
				.body("[0].user", equalTo("adam"))
				.and()
				.body("[0].description", equalTo("buy milk"));
	}

	@Test
	void testMarkDone() {
		int buyMilkId = given()
				.body("{\"description\":\"buy milk\"}")
				.contentType(ContentType.JSON)
				.post("/adam/todo")
				.then()
				.extract().body().path("id");

		when()
				.delete("/adam/todo/" + buyMilkId);

		when()
				.get("/adam/todo")
		.then()
				.body("$.size()", is(0));
	}
}
