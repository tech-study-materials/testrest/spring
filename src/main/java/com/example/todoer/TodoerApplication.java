package com.example.todoer;

import lombok.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.stream.Collectors.toUnmodifiableList;

@SpringBootApplication
@RestController
public class TodoerApplication implements TodoerApi {

	public static void main(String[] args) {
		SpringApplication.run(TodoerApplication.class, args);
	}

	@Bean
	Repo repo() {
		return new InMemRepo();
	}

	@Override
	public IdResponse createTodo(String user, NewTodoRequest req) {
		return new IdResponse(repo().create(user, req.getDescription()));
	}

	@Override
	public List<Todo> getTodos(String user) {
		return repo().findByUser(user);
	}

	@Override
	public void markDone(String user, long id) {
		repo().deleteById(id);
	}
}


//let's define REST API:
interface TodoerApi {
	@PostMapping("/{user}/todo")
	IdResponse createTodo(@PathVariable String user, @RequestBody NewTodoRequest req);

	@GetMapping("/{user}/todo")
	List<Todo> getTodos(@PathVariable String user);

	@DeleteMapping("/{user}/todo/{id}")
	void markDone(@PathVariable String user, @PathVariable long id);
}

//Let's define models:
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class Todo {
	long id;
	String user;
	String description;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class NewTodoRequest {
	String description;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class IdResponse {
	long id;
}

//let's define persistence
interface Repo {
	long create(String user, String description);
	List<Todo> findByUser(String user);
	void deleteById(long id);
	void deleteAll();
}

class InMemRepo implements Repo {
	final AtomicLong sequence = new AtomicLong();
	List<Todo> todos = new ArrayList<>();

	@Override
	public long create(String user, String description) {
		long id = sequence.incrementAndGet();
		todos.add(new Todo(id, user, description));
		return id;
	}

	@Override
	public List<Todo> findByUser(String user) {
		return todos.stream()
				.filter(t -> user.equals(t.user))
				.collect(toUnmodifiableList());
	}

	@Override
	public void deleteById(long id) {
		todos.stream()
				.filter(t -> id == t.id)
				.findFirst()
				.ifPresent(t -> todos.remove(t));
	}

	@Override
	public void deleteAll() {
		todos.clear();
	}
}